"use client"
import Image from "next/image";
import styles from "./page.module.css";
import Navbar from "@/components/Navbar/Navbar";
import BlogCard from "@/components/blogcards/BlogCard";
import BlogsSlider from "@/components/blogcards/BlogSlider";
// import ProfilePage from "@/components/BlogOptions/ProfilePage";
import ContactForm from "@/components/Bot/ContactForm";
import EmbeddedPage from "@/components/embeddedPage/embeddedPage";


export default function Profile() {
  return (
    <main>
      <Navbar/>
      <h1>.</h1>
      <h1>.</h1>
      {/* <BlogsSlider /> */}
      {/* <ProfilePage /> */}
      <ContactForm />
      <EmbeddedPage />
    </main>
  );
}
