"use client"
import Image from "next/image";
import styles from "./page.module.css";
import Navbar from "@/components/Navbar/Navbar";
import HighSubscriberChannels from "../youtubeanalysis/youtubeanalysis";

export default function Profile() {
  return (
    <main>
      <Navbar/>
      <h1>.</h1>
      <h1 style={{
                    fontSize: "20px",
                    fontWeight: "400",
                    margin: "10px 5px",
                    color: "white",
                    textDecoration: "none"
                }}>.</h1>
      <HighSubscriberChannels/>
    </main>
  );
}