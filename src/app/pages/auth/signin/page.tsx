"use client"
import React, { useState } from 'react';
import Image from 'next/image';
import styles from './page.module.css';
import Navbar from '@/components/Navbar/Navbar';
import '../auth.css';
import Link from 'next/link';
import { toast } from 'react-toastify';
import logo from '@/assets/BLOG.png';
// import { getCookie , setCookie} from 'cookies-next';
// Define an interface for the form data
interface FormData {
    email: string;
    password: string;
}

export default function Signin() {
    const [formData, setFormData] = useState<FormData>({
        email: '',
        password: '',
    });
    const [errors, setErrors] = useState<Record<string, string>>({});
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };


    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    
        // Validation checks...
    
        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_API}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
                credentials: 'include'
            });
    
            const responseData = await response.json();
    
            if (response.ok) {
                toast(responseData.message, {
                    type: 'success',
                    position: 'top-right',
                    autoClose: 2000
                });
                setErrors({}); // Clear errors
                
                console.log("Login Response Data:", responseData);
    
                try {
                    const checkLoginResponse = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_API}/auth/checklogin`, {
                        method: 'GET',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        credentials: 'include'
                    });
    
                    const checkLoginData = await checkLoginResponse.json();
    
                    console.log("Check Login Response:", checkLoginData);
    
                    if (checkLoginData.ok) {
                        console.log("Redirecting to root page...");
                        window.location.href = "/"; // Redirect to root page
                    } else {
                        console.log("Login check failed.");
                    }
                } catch (error) {
                    console.error("Error checking login:", error);
                    window.location.href = "/"; // Redirect to root page on error
                }
            } else {
                toast(responseData.message, {
                    type: 'error',
                    position: 'top-right',
                    autoClose: 2000
                });
            }
        } catch (error) {
            console.error("Login Error:", error);
            toast("Error logging in", {
                type: 'error',
                position: 'top-right',
                autoClose: 2000
            });
        }
    };
    

    // const checkLogin = async () => {
        
    // };
    

    return (
        <div className='authout'>
            <Navbar />
            <div className='authin'>
            <div className="left">
                    <Image src={logo} alt="" className='img' />
                </div>
                <div className='right'>
                    <form
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                        }}
                        onSubmit={handleSubmit}
                    >
                        <div className="forminput_cont">
                            <label>Email</label>
                            <input
                                type="text"
                                placeholder="Enter Your Email"
                                name="email"
                                value={formData.email}
                                onChange={handleChange}
                            />
                            {errors.email && <span className="formerror">{errors.email}</span>}
                        </div>
                        <div className="forminput_cont">
                            <label>Password</label>
                            <input
                                type="password"
                                placeholder="Enter Your Password"
                                name="password"
                                value={formData.password}
                                onChange={handleChange}
                            />
                            {errors.password && (
                                <span className="formerror">{errors.password}</span>
                            )}
                        </div>

                        <button type="submit" className="main_button">
                            Login
                        </button>

                        <p className="authlink">
                            Don't have an account? <Link href="/pages/auth/signup">Register</Link>
                        </p>
                    </form>
                </div>

            </div>
        </div >
    )
}