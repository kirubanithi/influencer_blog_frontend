"use client"
import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'next/navigation';
import { toast } from 'react-toastify';
import Navbar from '@/components/Navbar/Navbar';
import './blogPage.css';
import Image from 'next/image';
import BlogsSlider from '@/components/blogcards/BlogSlider';
import Footer from '@/components/Footer/Footer';
import ClockLoader from "react-spinners/ClockLoader";
import EmbeddedPage from '@/components/embeddedPage/embeddedPage';


interface ParagraphData {
    _id: string;
    title: string;
    description: string;
    imageUrl: string;
    position: string;
    createdAt: Number | null;
}

interface Blog {
    _id: string;
    title: string;
    description: string;
    imageUrl: string;
    paragraphs: ParagraphData[];
    category: string;
    owner: string;
    createdAt: string;
    updatedAt: string;
}

const BlogPage = () => {
    const searchParams = useSearchParams();
    const blogid = searchParams.get('blogid');
    console.log(blogid);

    const [blog, setBlog] = useState<Blog>({
        _id: '',
        title: '',
        description: '',
        imageUrl: '',
        paragraphs: [],
        category: '',
        owner: '',
        createdAt: '',
        updatedAt: ''
    });

    const [blogcreatedat, setBlogcreatedat] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        if (blogid) {
            getBlogbyId();
            window.scrollTo(0, 0);
        }
    }, [blogid]); // Dependency array to re-run effect when blogid changes

    async function getBlogbyId() {
        try {
            setLoading(true);
            const response = await fetch(`${process.env.NEXT_PUBLIC_BACKEND_API}/blog/${blogid}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                }
            });
            if (!response.ok) {
                throw new Error('Failed to fetch blog data');
            }
            const data = await response.json();
            console.log(data.blog);
            const { blog } = data;
            setBlog(blog);
            const formattedDate = formatDate(blog.createdAt);
            setBlogcreatedat(formattedDate);
        } catch (error: any) {
            toast(error.message, { type: 'error' });
        } finally {
            setLoading(false);
        }
    }

    function formatDate(inputDate: string) {
        const date = new Date(inputDate);
        const day = date.getDate();
        const monthNames = [
            'January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August',
            'September', 'October', 'November', 'December'
        ];
        const monthIndex = date.getMonth();
        const year = date.getFullYear();

        return `${day} ${monthNames[monthIndex]} ${year}`;
    }

    return (
        <div>
            <Navbar />
            <div className='blogpage-out'>
                {loading && <ClockLoader color="#36d7b7" loading={loading} size={150} />}
                {blog._id && (
                    <>
                        {/* Render blog content using the state variables */}
                        {
                loading && blog._id == '' ?
                    <div className='loaderfullpage'>
                        <ClockLoader
                            color="#36d7b7"
                            loading={loading}
                            size={150}
                            aria-label="Loading Spinner"
                            data-testid="loader"
                        />
                    </div>
                    :
                    <div className='blogpage'>
                        <div className='c1'>
                            <p className='createdat'>Created at {blogcreatedat}</p>
                            <p className='title'>{blog.title}</p>
                            <p className='category'>{blog.category}</p>

                           {
                            blog.imageUrl.length>0 && 
                            <Image src={blog.imageUrl} alt={blog.title} width={100} height={100} className='blogimg' unoptimized />
                           }
                            <p className='description'>{blog.description}</p>
                        </div>

                        <EmbeddedPage username={blog.title} title={blog.title} />

                        {
                            blog.paragraphs.map((paragraph: ParagraphData, index) => (
                                <div className={
                                    index % 2 === 0 ? 'c2left' : 'c2right'
                                } key={index}>
                                    {
                                        paragraph.imageUrl.length > 0 &&
                                        <Image src={paragraph.imageUrl} alt={blog.title} width={100} height={100}
                                            className='paraimg' unoptimized />
                                    }
                                    <div>
                                        <p className='title'>{paragraph.title}</p>
                                        <p className='description'>{paragraph.description}</p>
                                    </div>
                                    
                                </div>
                            ))
                        }
                        
                    </div>
            }
                    </>
                )}
            </div>
            <BlogsSlider />
            <Footer />
        </div>
    );
};

export default BlogPage;
