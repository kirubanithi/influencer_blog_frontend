"use client"
import Image from "next/image";
import styles from "./page.module.css";
import Navbar from "@/components/Navbar/Navbar";
import HomeSlider from "@/components/HomeSlider/HomeSlider";
import CategoriesSlider from "@/components/Categories/CategoriesSlider";
import BlogSlider from "@/components/blogcards/BlogSlider";
// import HighSubscriberChannels from "./pages/youtubeanalysis/youtubeanalysis";

export default function Home() {
  return (
    <main style={{ backgroundColor: "black" }}>
      <Navbar/>
      <h1>.</h1>
      <HomeSlider/>
      <CategoriesSlider/>
      <BlogSlider/>
      {/* <HighSubscriberChannels/> */}
      <h1>-----Footer-----</h1>
    </main>
  );
}
