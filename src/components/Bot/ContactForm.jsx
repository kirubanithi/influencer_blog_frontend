import React, { useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify'; // Import react-toastify
import styles from './ContactForm.module.css'; // Import CSS module for styling

const ContactForm = () => {
  const [showForm, setShowForm] = useState(false); // State to manage form visibility

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    creator_name: '',
    budget: '',
    platform: '',
    location: '',
    type: '',
    tenure_period: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${process.env.NEXT_PUBLIC_BACKEND_API}/auth/send-email`, formData);
      if (response.status === 200) {
        toast.success('Email sent successfully!'); // Display success toast
      }
    } catch (error) {
      console.error('Error sending email:', error);
      toast.error('Failed to send email. Please try again later.'); // Display error toast
    }
  };

  return (
    <div>
    <button className={styles.contactNowButton} onClick={() => setShowForm(true)}>Contact Now</button>
    {showForm && (
       <div className={styles.modal}>
       <div className={styles.modalContent}>
         <span className={styles.close} onClick={() => setShowForm(false)}>&times;</span>
      <form onSubmit={handleSubmit} className={styles.formInputContainer}>
        {/* Form fields */}
          <div>
            <label htmlFor="name">Name:</label>
            <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="email">Email:</label>
            <input type="email" id="email" name="email" value={formData.email} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="creator_name">Creator Name:</label>
            <textarea id="message" name="message" value={formData.message} onChange={handleChange}></textarea>
          </div>
          <div>
            <label htmlFor="budget">Budget:</label>
            <input type="text" id="budget" name="budget" value={formData.budget} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="platform">Platform:</label>
            <input type="text" id="platform" name="platform" value={formData.platform} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="location">Location:</label>
            <input type="text" id="location" name="location" value={formData.location} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="type">Promotion Types:</label>
            <input type="text" id="type" name="type" value={formData.type} onChange={handleChange} />
          </div>
          <div>
            <label htmlFor="tenure_period">Tenure Period:</label>
            <input type="text" id="tenure_period" name="tenure_period" value={formData.tenure_period} onChange={handleChange} />
          </div>
        <button type="submit">Submit</button>
      </form>
      </div>
      </div>
    )}
  </div>
    // <form onSubmit={handleSubmit}>
      // <div>
      //   <label htmlFor="name">Name:</label>
      //   <input type="text" id="name" name="name" value={formData.name} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="email">Email:</label>
      //   <input type="email" id="email" name="email" value={formData.email} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="creator_name">Creator Name:</label>
      //   <textarea id="message" name="message" value={formData.message} onChange={handleChange}></textarea>
      // </div>
      // <div>
      //   <label htmlFor="budget">Budget:</label>
      //   <input type="text" id="budget" name="budget" value={formData.budget} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="platform">Platform:</label>
      //   <input type="text" id="platform" name="platform" value={formData.platform} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="location">Location:</label>
      //   <input type="text" id="location" name="location" value={formData.location} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="type">Promotion Types:</label>
      //   <input type="text" id="type" name="type" value={formData.type} onChange={handleChange} />
      // </div>
      // <div>
      //   <label htmlFor="tenure_period">Tenure Period:</label>
      //   <input type="text" id="tenure_period" name="tenure_period" value={formData.tenure_period} onChange={handleChange} />
      // </div>
    //   <button type="submit">Submit</button>
    // </form>
  );
};

export default ContactForm;
