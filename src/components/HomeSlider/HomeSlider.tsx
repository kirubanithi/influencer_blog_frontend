import React from 'react'
import Image from 'next/image';
import img1 from "@/assets/sliderTemp/img1.png"
import img2 from "@/assets/sliderTemp/img2.png"

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
// Import Swiper styles
import 'swiper/css/pagination';
import 'swiper/css/navigation';
// import required modules
import { Pagination, Navigation } from 'swiper/modules';
import Link from 'next/link';

const width = window.innerWidth;
const height = window.innerHeight;

const HomeSlider = () => {
        return (
            <div>
                <Swiper
                    slidesPerView={1}
                    spaceBetween={30}
                    loop={true}
                    pagination={{
                    clickable: true,
                    }}
                    navigation={true}
                    modules={[Pagination, Navigation]}
                    className="mySwiper"
                >
                    {/* <SwiperSlide>
                        <Image src={img1} alt="" width={width} height={height / 2}
                            style={{
                                objectFit: "cover"
                            }} />
                    </SwiperSlide> */}
                
                    <SwiperSlide>
                        <Link id="link" href="/pages/yt">
                            <Image src={img1} alt="" width={width} height={height / 2}
                            style={{
                                objectFit: "cover"
                            }} />
                        </Link>
                    </SwiperSlide>


                    <SwiperSlide>
                        <Link id="link" href="/pages/yt">
                            <Image src={img1} alt="" width={width} height={height / 2}
                            style={{
                                objectFit: "cover"
                            }} />
                        </Link>
                    </SwiperSlide>
                </Swiper>
            </div>
        )
}

export default HomeSlider