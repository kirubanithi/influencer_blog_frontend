import React, { useEffect, useState } from 'react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
import BlogCard from './BlogCard';
import { toast } from 'react-toastify';

interface ParagraphData {
    title: string;
    description: string;
    image: File | null;
    imageUrl: string;
    position: string;
    createdAt: Number | null;
}

interface Blog {
    _id: string;
    title: string;
    description: string;
    image: File | null;
    imageUrl: string;
    paragraphs: ParagraphData[];
    category: string;
}

const BlogsSlider = () => {
    const [blogs, setBlogs] = useState<Blog[]>([]);

    const get10latestblogs = () => {
        fetch(`${process.env.NEXT_PUBLIC_BACKEND_API}/blog`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then((res) => {
            if (!res.ok) {
                throw new Error('Failed to fetch blogs');
            }
            return res.json();
        })
        .then((data) => {
            console.log(data);
            setBlogs(data.blogs);
        })
        .catch((error) => {
            console.error('Error fetching blogs:', error);
            toast.error('Error fetching blogs');
        });
    }

    useEffect(() => {
        get10latestblogs();
    }, []);

    return (
        <div className='sliderout'>
            <h1>Latest Blogs</h1>
            <Swiper
                slidesPerView={1}
                spaceBetween={1}
                pagination={{
                    clickable: true,
                }}
                breakpoints={{
                    '@0.00': {
                        slidesPerView: 1,
                        spaceBetween: 2,
                    },
                    '@0.75': {
                        slidesPerView: 2,
                        spaceBetween: 2,
                    },
                    '@1.00': {
                        slidesPerView: 3,
                        spaceBetween: 2,
                    },
                    '@1.50': {
                        slidesPerView: 5,
                        spaceBetween: 2,
                    },
                }}
                modules={[Pagination]}
                className="mySwiper"
            >
                {blogs.map((blog) => (
                    <SwiperSlide key={blog._id}>
                        <BlogCard {...blog} />
                    </SwiperSlide>
                ))}
            </Swiper>
        </div>
    )
}

export default BlogsSlider;
