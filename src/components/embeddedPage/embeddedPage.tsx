import React from 'react';
import './embeddedPage.css'; // Import the provided CSS file

interface EmbeddedPageProps {
  username: string;
  title: string;
}

const EmbeddedPage: React.FC<EmbeddedPageProps> = ({ username, title }) => {
  return (
    <div>
      <div className="iframeContainer">
        <h2>{title}</h2> {/* Display the blog title */}
        <iframe
          src={`https://socialblade.com/youtube/user/${username}`} 
          scrolling='no'
          className="iframeContent"/>
      </div>
      <h1> .</h1>
      <div className="iframeContainer1">
        <h2>{title}</h2> {/* Display the blog title */}
        <iframe
          src={`https://socialblade.com/youtube/user/${username}`} 
          scrolling='no'
          className="iframeContent1"/>
      </div>
    </div>

  );
};

export default EmbeddedPage;
